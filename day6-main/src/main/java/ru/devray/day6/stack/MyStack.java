package ru.devray.day6.stack;

public class MyStack<T> {

    T[] elements = (T[]) new Object[10]; //{e,null,null,null}
    int index = -1;

    //класть
    public void push(T element) {
        try {
            elements[++index] = element;
        } catch (ArrayIndexOutOfBoundsException numberObjectsDoesNotMatchStackSize) {
            System.out.println("Попытка положить в стек количество объектов превышающее размер стека");//обработка ошибки
            //numberObjectsDoesNotMatchStackSize.printStackTrace();
            throw numberObjectsDoesNotMatchStackSize;
        }
    }

    //брать
    public T pop() {
        try {
            T element = elements[index];
            elements[index--] = null;
            return element;
        } catch (ArrayIndexOutOfBoundsException numberObjectsDoesNotMatchStackSize) {
            System.out.println("Попытка получить из стека несуществующий объект");//обработка ошибки
            throw numberObjectsDoesNotMatchStackSize;
        }
    }
    /**
     * Дженерик, это что-то вроде шаблона в который на место "T" можно подставить любой ссылочный тип
     **/
}
